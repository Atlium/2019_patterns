package com.atlium;

import com.atlium.product.DrinkType;
import com.atlium.product.PizzaType;
import com.atlium.restataunt.factory.SamaraFoodFactory;
import com.atlium.restataunt.factory.SamaraRestaurantFactory;
import com.atlium.restataunt.model.*;
import com.atlium.restataunt.service.Restaurant;
import com.atlium.restataunt.model.RestaurantType;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Main {

    private static final String DATE_FORMAT = "dd.MM.yyyy";

    public static void main(String[] args) {
        System.out.println("Hello, patterns!");
        Restaurant restaurant = new SamaraRestaurantFactory(new SamaraFoodFactory()).getRestaurant(RestaurantType.ITALY);

        Human humanVasya = new Human.Builder("4444 555666", getDate("01.01.1970"), "Vasya")
                .withCard(new Card("5678 1234 ", 1000L))
                .withPhone("9279888765")
                .build();
        Order vasyaFirstOrder = new Order.Builder().withDrink(DrinkType.SPRITE).withPizza(PizzaType.CHEESE).build();
        Order vasyaSecondOrder = new Order.Builder().withDrink(DrinkType.BEAR).withPizza(PizzaType.MEAT).build();
        System.out.println();
        restaurant.eat(humanVasya, vasyaFirstOrder, PaymentType.CARD);
        System.out.println();
        restaurant.eat(humanVasya, vasyaSecondOrder, PaymentType.CASH);


        Human humanPavel = new Human.Builder("1111 222333", getDate("28.10.1992"), "Pavel")
                .withCard(new Card("1234 5678", 2000L))
                .withCash(new Cash(1000L))
                .withPhone("9271222345")
                .build();
        Order pavelFirstOrder = new Order.Builder().withDrink(DrinkType.COCA_COLA).withPizza(PizzaType.PEPPERONI).build();
        Order pavelSecondOrder = new Order.Builder().withDrink(DrinkType.COCA_COLA).withPizza(PizzaType.MEAT).build();
        restaurant.eat(humanPavel, pavelFirstOrder, PaymentType.CARD);

        System.out.println();
        restaurant.eat(humanPavel, pavelFirstOrder, PaymentType.CARD);
        System.out.println();
        restaurant.eat(humanPavel, pavelSecondOrder, PaymentType.CARD);
    }

    private static Date getDate(String formattedDate) {
        try {
            return new SimpleDateFormat(DATE_FORMAT).parse(formattedDate);
        } catch (ParseException e) {
            System.out.println(String.format("-Wrong date format, expected:%s, got:%s", DATE_FORMAT, formattedDate));
            return null;
        }
    }
}
