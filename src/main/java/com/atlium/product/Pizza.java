package com.atlium.product;

public interface Pizza extends Food {
    Long getSize();
}
