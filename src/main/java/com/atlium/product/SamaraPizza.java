package com.atlium.product;

public class SamaraPizza implements Pizza {

    private final PizzaType pizzaType;
    private final Long cost;
    private final Long size;

    public SamaraPizza(PizzaType pizzaType, Long cost, Long size) {
        this.pizzaType = pizzaType;
        this.cost = cost;
        this.size = size;
    }

    public PizzaType getPizzaType() {
        return pizzaType;
    }

    public Long getCost() {
        return cost;
    }

    @Override
    public Long getSize() {
        return size;
    }

    @Override
    public Long getPrise() {
        return getCost();
    }

    @Override
    public String toString() {
        return "SamaraPizza{" +
                "pizzaType=" + pizzaType +
                ", cost=" + cost +
                ", size=" + size +
                '}';
    }
}
