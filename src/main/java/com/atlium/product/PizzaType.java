package com.atlium.product;

public enum PizzaType {
    PEPPERONI, MEAT, CHEESE
}
