package com.atlium.product;

public class SamaraDrink implements Drink {

    private final DrinkType drinkType;
    private final Long cost;
    private final Double volume;

    public SamaraDrink(DrinkType drinkType, Long cost, Double volume) {
        this.drinkType = drinkType;
        this.cost = cost;
        this.volume = volume;
    }

    public DrinkType getDrinkType() {
        return drinkType;
    }

    public Long getCost() {
        return cost;
    }

    @Override
    public Double getVolume() {
        return volume;
    }

    @Override
    public Long getPrise() {
        return getCost();
    }

    @Override
    public String toString() {
        return "SamaraDrink{" +
                "drinkType=" + drinkType +
                ", cost=" + cost +
                ", volume=" + volume +
                '}';
    }
}
