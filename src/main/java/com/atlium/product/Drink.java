package com.atlium.product;

public interface Drink extends Food {
    Double getVolume();
}
