package com.atlium.exception;

public class NotEnoughMoneyException extends RuntimeException {
    private final Long moneyAmount;
    private final Long needMoney;

    public NotEnoughMoneyException(Long moneyAmount, Long needMoney) {
        this.moneyAmount = moneyAmount;
        this.needMoney = needMoney;
    }

    @Override
    public String getMessage() {
        return String.format("Not enough money, have:%d, need:%d", moneyAmount, needMoney);
    }
}
