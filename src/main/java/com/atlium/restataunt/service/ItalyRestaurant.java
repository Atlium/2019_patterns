package com.atlium.restataunt.service;

import com.atlium.restataunt.bar.Bar;
import com.atlium.restataunt.kitchen.Kitchen;
import com.atlium.restataunt.model.*;

import java.util.Map;
import java.util.Queue;
import java.util.stream.IntStream;

@SuppressWarnings("Duplicates")
public class ItalyRestaurant extends AbstractRestaurant {

    private static final double BONUS_MULTIPLIER = 0.01;
    private final Kitchen kitchen;
    private final Bar bar;
    private final Queue<Waiter> waiters;
    private final Map<String, Client> clientMap;
    private final PaymentTerminal paymentTerminal;
    private boolean isActiveBonusSystem;

    public ItalyRestaurant(Kitchen kitchen, Bar bar, Queue<Waiter> waiters, Map<String, Client> clientMap, PaymentTerminal paymentTerminal) {
        this.kitchen = kitchen;
        this.bar = bar;
        this.waiters = waiters;
        this.clientMap = clientMap;
        this.paymentTerminal = paymentTerminal;
    }

    @Override
    public Waiter getWaiter() {
        System.out.println("getWaiter#params:");
        Waiter waiter = waiters.poll();
        while (waiter == null) {
            System.out.println("Not enough waiters at this moment!");
            waiter = waiters.peek();
        }
        System.out.println(String.format("-getWaiter#return:%s", waiter));
        return waiter;
    }

    @Override
    public void releaseWaiter(Waiter waiter) {
        waiters.offer(waiter);
    }

    @Override
    public FoodOrder makeOrder(Waiter waiter, Order order) {
        System.out.println(String.format("makeOrder#params:waiter:%s, order:%s", waiter, order));
        waiter.collectFood(kitchen.createPizza(order.getPizza()));
        waiter.collectFood(bar.createDrink(order.getDrink()));
        FoodOrder foodOrder = waiter.deliveryFoodOrder();
        System.out.println(String.format("-makeOrder#return:%s", foodOrder));
        return foodOrder;
    }

    @Override
    public void makePayment(Human human, FoodOrder foodOrder, PaymentType paymentType) {
        System.out.println(String.format("makePayment#params:human:%s, foodOrder:%s, paymentType:%s", human, foodOrder, paymentType));
        Client client = clientMap.computeIfAbsent(human.getPhone(), phone -> new Client(0L, phone));
        if (clientMap.size() > 1) {
            isActiveBonusSystem = true;
        }
        System.out.println(String.format("-makePayment#beforePayment:name:%s, card:%s, cash:%s, bonuses:%s",
                human.getName(),
                human.getCard() == null ? 0 : human.getCard().getMoneyAmount(),
                human.getCash() == null ? 0 : human.getCash().getMoneyAmount(),
                client.getBonuses()));
        Long price = foodOrder.getPrice();
        try {
            if (isActiveBonusSystem) {
                Long allowedBonuses = getAllowedBonusesByPrice(client, price);
                Long priceWithBonuses = price - allowedBonuses;
                makePayment(paymentType, human, priceWithBonuses);
                client.debit(allowedBonuses);
                client.addBonuses(calculateBonuses(priceWithBonuses));
            } else {
                makePayment(paymentType, human, price);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        System.out.println(String.format("-makePayment#afterPayment:name:%s, card:%s, cash:%s, bonuses:%s",
                human.getName(),
                human.getCard() == null ? 0 : human.getCard().getMoneyAmount(),
                human.getCash() == null ? 0 : human.getCash().getMoneyAmount(),
                client.getBonuses()));
    }

    private void makePayment(PaymentType paymentType, Human human, Long price) {
        switch (paymentType) {
            case CARD:
                paymentTerminal.payByCard(human.getCard(), price);
                break;
            case CASH:
                paymentTerminal.payByCash(human.getCash(), price);
                break;
        }
    }

    private Long getAllowedBonusesByPrice(Client client, Long price) {
        Long bonuses = client.getBonuses();//if add ClientType we can change amount of bonuses here
        long threshold = price / 2;
        return bonuses > threshold ? threshold : bonuses;
    }

    private Long calculateBonuses(Long price) {
        return Math.round(price * BONUS_MULTIPLIER); //and here
    }

    public void hireWaiters(int count) {
        IntStream.range(0, count).forEach(index -> waiters.offer(new Waiter()));
    }
}
