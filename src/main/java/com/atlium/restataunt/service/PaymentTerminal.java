package com.atlium.restataunt.service;

import com.atlium.restataunt.model.Card;
import com.atlium.restataunt.model.Cash;
import com.atlium.restataunt.model.Client;

public interface PaymentTerminal {
    void payByCard(Card card, Long amount);

    Long payByCash(Cash cash, Long payAmount);
}
