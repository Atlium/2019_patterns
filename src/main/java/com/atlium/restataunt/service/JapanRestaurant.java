package com.atlium.restataunt.service;

import com.atlium.restataunt.bar.Bar;
import com.atlium.restataunt.kitchen.Kitchen;
import com.atlium.restataunt.model.*;

import java.util.Map;
import java.util.Queue;
import java.util.stream.IntStream;

/**
 * Works without bonuses
 */
@SuppressWarnings("Duplicates")
public class JapanRestaurant extends AbstractRestaurant {

    private final Kitchen kitchen;
    private final Bar bar;
    private final Queue<Waiter> waiters;
    private final Map<String, Client> clientMap;
    private final PaymentTerminal paymentTerminal;

    public JapanRestaurant(Kitchen kitchen, Bar bar, Queue<Waiter> waiters, Map<String, Client> clientMap, PaymentTerminal paymentTerminal) {
        this.kitchen = kitchen;
        this.bar = bar;
        this.waiters = waiters;
        this.clientMap = clientMap;
        this.paymentTerminal = paymentTerminal;
    }

    @Override
    public Waiter getWaiter() {
        System.out.println("getWaiter#params:");
        Waiter waiter = waiters.poll();
        while (waiter == null) {
            System.out.println("Not enough waiters at this moment!");
            waiter = waiters.peek();
        }
        System.out.println(String.format("-getWaiter#return:%s", waiter));
        return waiter;
    }

    @Override
    public void releaseWaiter(Waiter waiter) {
        waiters.offer(waiter);
    }

    @Override
    public FoodOrder makeOrder(Waiter waiter, Order order) {
        System.out.println(String.format("makeOrder#params:waiter:%s, order:%s", waiter, order));
        waiter.collectFood(kitchen.createPizza(order.getPizza()));
        waiter.collectFood(bar.createDrink(order.getDrink()));
        FoodOrder foodOrder = waiter.deliveryFoodOrder();
        System.out.println(String.format("-makeOrder#return:%s", foodOrder));
        return foodOrder;
    }

    @Override
    public void makePayment(Human human, FoodOrder foodOrder, PaymentType paymentType) {
        System.out.println(String.format("makePayment#params:human:%s, foodOrder:%s, paymentType:%s", human, foodOrder, paymentType));
        Client client = clientMap.computeIfAbsent(human.getPhone(), phone -> new Client(0L, phone));
        System.out.println(String.format("-makePayment#beforePayment:name:%s, card:%s, cash:%s, bonuses:%s",
                human.getName(),
                human.getCard() == null ? 0 : human.getCard().getMoneyAmount(),
                human.getCash() == null ? 0 : human.getCash().getMoneyAmount(),
                client.getBonuses()));
        Long price = foodOrder.getPrice();
        try {
            switch (paymentType) {
                case CARD:
                    paymentTerminal.payByCard(human.getCard(), price);
                    break;
                case CASH:
                    paymentTerminal.payByCash(human.getCash(), price);
                    break;
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        System.out.println(String.format("-makePayment#afterPayment:name:%s, card:%s, cash:%s, bonuses:%s",
                human.getName(),
                human.getCard() == null ? 0 : human.getCard().getMoneyAmount(),
                human.getCash() == null ? 0 : human.getCash().getMoneyAmount(),
                client.getBonuses()));
    }

    public void hireWaiters(int count) {
        IntStream.range(0, count).forEach(index -> waiters.offer(new Waiter()));
    }
}
