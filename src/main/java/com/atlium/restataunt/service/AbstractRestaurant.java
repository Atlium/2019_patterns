package com.atlium.restataunt.service;

import com.atlium.restataunt.model.*;

public abstract class AbstractRestaurant implements Restaurant {
    @Override
    public final void eat(Human human, Order order, PaymentType paymentType) {
        System.out.println(String.format("makeOrder#params:human:%s,order:%s", human, order));
        Waiter waiter = getWaiter();
        FoodOrder foodOrder = makeOrder(waiter, order);
        makePayment(human, foodOrder, paymentType);
        releaseWaiter(waiter);
    }
}
