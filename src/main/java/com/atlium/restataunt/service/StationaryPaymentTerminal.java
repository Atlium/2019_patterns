package com.atlium.restataunt.service;

import com.atlium.exception.NotEnoughMoneyException;
import com.atlium.restataunt.model.Card;
import com.atlium.restataunt.model.Cash;

public class StationaryPaymentTerminal implements PaymentTerminal {

    @Override
    public void payByCard(Card card, Long amount) {
        if (card == null) {
            System.out.println("Wrong card:" + card);
            return;
        }
        try {
            card.pay(amount);
        } catch (NotEnoughMoneyException e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public Long payByCash(Cash cash, Long payAmount) {
        if (cash == null) {
            System.out.println("Wrong cash:" + cash);
            return 0L;
        }
        try {
            Long moneyAmount = cash.getMoneyAmount();
            cash.pay(payAmount);
            return moneyAmount - payAmount;
        } catch (NotEnoughMoneyException e) {
            System.out.println(e.getMessage());
            return 0L;
        }
    }
}
