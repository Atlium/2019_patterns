package com.atlium.restataunt.service;

import com.atlium.restataunt.model.*;

public interface Restaurant {

    void eat(Human human, Order order, PaymentType paymentType);

    Waiter getWaiter();

    void releaseWaiter(Waiter waiter);

    FoodOrder makeOrder(Waiter waiter, Order order);

    void makePayment(Human human, FoodOrder foodOrder, PaymentType paymentType);

    void hireWaiters(int count);
}
