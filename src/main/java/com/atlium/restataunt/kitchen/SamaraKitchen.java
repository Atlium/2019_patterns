package com.atlium.restataunt.kitchen;

import com.atlium.product.Pizza;
import com.atlium.product.PizzaType;

import java.util.Queue;
import java.util.stream.IntStream;

public class SamaraKitchen implements Kitchen {

    private final Queue<PizzaMaker> pizzaMakers;
    private final PizzaShop pizzaShop;

    public SamaraKitchen(Queue<PizzaMaker> pizzaMakers, PizzaShop pizzaShop) {
        this.pizzaMakers = pizzaMakers;
        this.pizzaShop = pizzaShop;
    }

    @Override
    public Pizza createPizza(PizzaType pizzaType) {
        PizzaMaker pizzaMaker = pizzaMakers.peek();
        if (pizzaMaker == null) {
            return pizzaShop.salePizza(pizzaType);
        }
        Pizza pizza = pizzaMaker.makePizza(pizzaType);
        pizzaMakers.offer(pizzaMaker);
        return pizza;
    }

    @Override
    public void hirePizzaMakers(int count) {
        IntStream.range(0, count).forEach(index -> pizzaMakers.offer(new SamaraPizzaMaker()));
    }
}
