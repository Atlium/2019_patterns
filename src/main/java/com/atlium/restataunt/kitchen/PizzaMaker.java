package com.atlium.restataunt.kitchen;

import com.atlium.product.Pizza;
import com.atlium.product.PizzaType;

public interface PizzaMaker {
    Pizza makePizza(PizzaType pizzaType);
}
