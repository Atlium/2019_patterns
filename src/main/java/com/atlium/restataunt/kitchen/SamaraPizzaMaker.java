package com.atlium.restataunt.kitchen;

import com.atlium.product.Pizza;
import com.atlium.product.PizzaType;
import com.atlium.product.SamaraPizza;

public class SamaraPizzaMaker implements PizzaMaker {
    private static Long nextId = 0L;

    private Long id;

    public SamaraPizzaMaker() {
        id = getNextId();
    }

    @Override
    public Pizza makePizza(PizzaType pizzaType) {
        SamaraPizza samaraPizza = new SamaraPizza(pizzaType, 300L, 35L);
        System.out.println(String.format("-PizzaMaker id:%s make pizza:%s", id, samaraPizza));
        return samaraPizza;
    }

    private static Long getNextId() {
        Long id = null;
        synchronized (SamaraPizzaMaker.class) {
            id = nextId++;
        }
        return id;
    }
}
