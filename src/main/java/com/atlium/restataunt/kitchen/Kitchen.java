package com.atlium.restataunt.kitchen;

import com.atlium.product.Pizza;
import com.atlium.product.PizzaType;

public interface Kitchen {
    Pizza createPizza(PizzaType pizzaType);

    void hirePizzaMakers(int count);
}
