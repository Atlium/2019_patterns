package com.atlium.restataunt.kitchen;

import com.atlium.product.Pizza;
import com.atlium.product.PizzaType;
import com.atlium.product.SamaraPizza;

public class SamaraPizzaShop implements PizzaShop {
    @Override
    public Pizza salePizza(PizzaType pizzaType) {
        return new SamaraPizza(pizzaType, 300L, 35L);
    }
}
