package com.atlium.restataunt;

public final class EmployeeCount {

    public static final int WAITERS = 20;
    public static final int KITCHEN_WORKERS = 20;

    private EmployeeCount() {
        //constants
    }
}
