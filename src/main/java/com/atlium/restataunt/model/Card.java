package com.atlium.restataunt.model;

import com.atlium.exception.NotEnoughMoneyException;

public class Card implements Payment {
    private final String id;
    private Long moneyAmount;

    public Card(String id, Long moneyAmount) {
        this.id = id;
        this.moneyAmount = moneyAmount;
    }

    public String getId() {
        return id;
    }

    public Long getMoneyAmount() {
        return moneyAmount;
    }

    public void setMoneyAmount(Long moneyAmount) {
        this.moneyAmount = moneyAmount;
    }

    @Override
    public void pay(Long amount) {
        if (amount == null) {
            throw new IllegalArgumentException(String.format("Wrong amount for pay:%d", amount));
        }
        if (moneyAmount < amount) {
            throw new NotEnoughMoneyException(moneyAmount, amount);
        }
        moneyAmount = moneyAmount - amount;
    }

    @Override
    public String toString() {
        return "Card{" +
                "id='" + id + '\'' +
                ", moneyAmount=" + moneyAmount +
                '}';
    }
}
