package com.atlium.restataunt.model;

public enum PaymentType {
    CASH, CARD
}
