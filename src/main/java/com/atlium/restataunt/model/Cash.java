package com.atlium.restataunt.model;

import com.atlium.exception.NotEnoughMoneyException;

public class Cash implements Payment {
    private Long moneyAmount;

    public Cash(Long moneyAmount) {
        this.moneyAmount = moneyAmount;
    }

    public Long getMoneyAmount() {
        return moneyAmount;
    }

    public void setMoneyAmount(Long moneyAmount) {
        this.moneyAmount = moneyAmount;
    }


    @Override
    public void pay(Long amount) {
        if (amount == null) {
            throw new IllegalArgumentException(String.format("Wrong amount for pay:%d", amount));
        }
        if (moneyAmount < amount) {
            throw new NotEnoughMoneyException(moneyAmount, amount);
        }
        moneyAmount = moneyAmount - amount;
    }

    @Override
    public String toString() {
        return "Cash{" +
                "moneyAmount=" + moneyAmount +
                '}';
    }
}
