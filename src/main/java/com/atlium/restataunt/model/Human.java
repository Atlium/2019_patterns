package com.atlium.restataunt.model;

import java.util.Date;
import java.util.Objects;

public class Human {
    private final String passportId;
    private final Date birthDate;
    private final String name;
    private String phone;
    private Cash cash;
    private Card card;

    private Human(Builder builder) {
        this.passportId = builder.passportId;
        this.birthDate = builder.birthDate;
        this.name = builder.name;
        this.phone = builder.phone;
        this.cash = builder.cash;
        this.card = builder.card;
    }

    public static class Builder {
        private final String passportId;
        private final Date birthDate;
        private final String name;
        private String phone;
        private Cash cash;
        private Card card;

        public Builder(String passportId, Date birthDate, String name) {
            if (passportId == null || birthDate == null || name == null) {
                throw new IllegalArgumentException(
                        String.format("-Wrong human data: passportId:%s, birthDate:%s, name:%s", passportId, birthDate, name));
            }
            this.passportId = passportId;
            this.birthDate = birthDate;
            this.name = name;
        }

        public Builder withPhone(String phone) {
            this.phone = phone;
            return this;
        }

        public Builder withCash(Cash cash) {
            this.cash = cash;
            return this;
        }

        public Builder withCard(Card card) {
            this.card = card;
            return this;
        }

        public Human build() {
            return new Human(this);
        }
    }

    public String getPassportId() {
        return passportId;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Cash getCash() {
        return cash;
    }

    public void setCash(Cash cash) {
        this.cash = cash;
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Human)) return false;
        Human human = (Human) o;
        return Objects.equals(getPassportId(), human.getPassportId());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getPassportId());
    }

    @Override
    public String toString() {
        return "Human{" +
                "passportId='" + passportId + '\'' +
                ", birthDate=" + birthDate +
                ", name='" + name + '\'' +
                ", phone='" + phone + '\'' +
                ", cash=" + cash +
                ", card=" + card +
                '}';
    }
}
