package com.atlium.restataunt.model;

import com.atlium.product.DrinkType;
import com.atlium.product.PizzaType;

public class Order {
    private final PizzaType pizza;
    private final DrinkType drink;

    private Order(Builder builder) {
        this.pizza = builder.pizza;
        this.drink = builder.drink;
    }

    public static class Builder {
        private PizzaType pizza;
        private DrinkType drink;

        public Builder() {
        }

        public Builder withPizza(PizzaType pizza) {
            this.pizza = pizza;
            return this;
        }

        public Builder withDrink(DrinkType drink) {
            this.drink = drink;
            return this;
        }

        public Order build() {
            return new Order(this);
        }
    }

    public PizzaType getPizza() {
        return pizza;
    }

    public DrinkType getDrink() {
        return drink;
    }

    @Override
    public String toString() {
        return "Order{" +
                "pizza=" + pizza +
                ", drink=" + drink +
                '}';
    }
}
