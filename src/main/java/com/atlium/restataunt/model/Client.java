package com.atlium.restataunt.model;

import java.util.Objects;

public class Client {
    private Long bonuses;
    private final String phone;

    public Client(Long bonuses, String phone) {
        this.bonuses = bonuses;
        this.phone = phone;
    }

    public Long getBonuses() {
        return bonuses;
    }


    public void setBonuses(Long bonuses) {
        this.bonuses = bonuses;
    }

    /**
     * @param amount to debit
     * @return amount after debit
     */
    public Long debit(Long amount) {
        if (bonuses > amount) {
            bonuses = bonuses - amount;
            return 0L;
        }
        Long oldBonuses = bonuses;
        bonuses = 0L;
        return amount - oldBonuses;
    }

    public void addBonuses(Long amount) {
        bonuses = bonuses + amount;
    }

    public String getPhone() {
        return phone;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Client)) return false;
        Client client = (Client) o;
        return Objects.equals(getPhone(), client.getPhone());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getPhone());
    }

    @Override
    public String toString() {
        return "Client{" +
                "bonuses=" + bonuses +
                ", phone='" + phone + '\'' +
                '}';
    }
}
