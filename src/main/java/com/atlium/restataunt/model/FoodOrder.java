package com.atlium.restataunt.model;

import com.atlium.product.Food;

import java.util.List;

public class FoodOrder {
    private List<Food> foodList;

    public FoodOrder(List<Food> foodList) {
        this.foodList = foodList;
    }

    public List<Food> getFoodList() {
        return foodList;
    }

    public Long getPrice() {
        return foodList
                .stream()
                .map(Food::getPrise)
                .reduce((a, b) -> a + b)
                .orElseThrow(() -> new RuntimeException(String.format("Wrong foodOrder:%s", foodList)));
    }

    @Override
    public String toString() {
        return "FoodOrder{" +
                "foodList=" + foodList +
                '}';
    }
}
