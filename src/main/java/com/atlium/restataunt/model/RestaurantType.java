package com.atlium.restataunt.model;

public enum RestaurantType {
    ITALY, JAPAN
}
