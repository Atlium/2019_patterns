package com.atlium.restataunt.model;

public interface Payment {
    void pay(Long amount);
}
