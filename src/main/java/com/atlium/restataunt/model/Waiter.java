package com.atlium.restataunt.model;

import com.atlium.product.Food;

import java.util.ArrayList;

/**
 * Официант
 */
public class Waiter {

    private static Long nextId = 0L;

    private Long id;
    private FoodOrder foodOrder;

    public Waiter() {
        id = getNextId();
        foodOrder = new FoodOrder(new ArrayList<>());
    }

    public void collectFood(Food food) {
        System.out.println(String.format("-Waiter id:%s collect food:%s", id, food));
        foodOrder.getFoodList().add(food);
    }

    public FoodOrder deliveryFoodOrder() {
        FoodOrder oldOrder = foodOrder;
        foodOrder = new FoodOrder(new ArrayList<>());
        System.out.println(String.format("-Waiter id:%s deliver order:%s", id, foodOrder));
        return oldOrder;
    }

    private static Long getNextId() {
        Long id = null;
        synchronized (Waiter.class) {
            id = nextId++;
        }
        return id;
    }

    @Override
    public String toString() {
        return "Waiter{" +
                "id=" + id +
                '}';
    }
}
