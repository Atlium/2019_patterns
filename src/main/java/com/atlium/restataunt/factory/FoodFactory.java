package com.atlium.restataunt.factory;

import com.atlium.restataunt.bar.Bar;
import com.atlium.restataunt.kitchen.Kitchen;

public interface FoodFactory {
    Bar createBar();

    Kitchen createKitchen();
}
