package com.atlium.restataunt.factory;

import com.atlium.restataunt.service.Restaurant;
import com.atlium.restataunt.model.RestaurantType;

public interface RestaurantFactory {
    Restaurant getRestaurant(RestaurantType restaurantType);
}
