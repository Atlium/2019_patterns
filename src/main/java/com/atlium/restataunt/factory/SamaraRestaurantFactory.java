package com.atlium.restataunt.factory;

import com.atlium.restataunt.EmployeeCount;
import com.atlium.restataunt.service.ItalyRestaurant;
import com.atlium.restataunt.service.JapanRestaurant;
import com.atlium.restataunt.service.Restaurant;
import com.atlium.restataunt.model.RestaurantType;
import com.atlium.restataunt.service.StationaryPaymentTerminal;

import java.util.ArrayDeque;
import java.util.HashMap;

public class SamaraRestaurantFactory {

    private final FoodFactory foodFactory;

    public SamaraRestaurantFactory(FoodFactory foodFactory) {
        this.foodFactory = foodFactory;
    }

    public Restaurant getRestaurant(RestaurantType restaurantType) {
        if (restaurantType == RestaurantType.ITALY) {
            Restaurant restaurant = new ItalyRestaurant(foodFactory.createKitchen(),
                    foodFactory.createBar(),
                    new ArrayDeque<>(EmployeeCount.WAITERS),
                    new HashMap<>(),
                    new StationaryPaymentTerminal());
            restaurant.hireWaiters(EmployeeCount.WAITERS);
            return restaurant;
        }
        Restaurant restaurant = new JapanRestaurant(foodFactory.createKitchen(),
                foodFactory.createBar(),
                new ArrayDeque<>(EmployeeCount.WAITERS),
                new HashMap<>(),
                new StationaryPaymentTerminal());
        restaurant.hireWaiters(EmployeeCount.WAITERS);
        return restaurant;
    }
}
