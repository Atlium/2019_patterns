package com.atlium.restataunt.factory;

import com.atlium.restataunt.EmployeeCount;
import com.atlium.restataunt.bar.Bar;
import com.atlium.restataunt.bar.SamaraBar;
import com.atlium.restataunt.kitchen.Kitchen;
import com.atlium.restataunt.kitchen.SamaraKitchen;
import com.atlium.restataunt.kitchen.SamaraPizzaShop;

import java.util.ArrayDeque;

public class SamaraFoodFactory implements FoodFactory {

    @Override
    public Bar createBar() {
        return new SamaraBar();
    }

    @Override
    public Kitchen createKitchen() {
        SamaraKitchen samaraKitchen = new SamaraKitchen(new ArrayDeque<>(EmployeeCount.KITCHEN_WORKERS), new SamaraPizzaShop());
        samaraKitchen.hirePizzaMakers(EmployeeCount.KITCHEN_WORKERS);
        return samaraKitchen;
    }
}
