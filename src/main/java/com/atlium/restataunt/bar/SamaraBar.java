package com.atlium.restataunt.bar;

import com.atlium.product.Drink;
import com.atlium.product.DrinkType;
import com.atlium.product.SamaraDrink;

public class SamaraBar implements Bar {
    @Override
    public Drink createDrink(DrinkType drinkType) {
        return new SamaraDrink(drinkType, 100L, 0.5);
    }
}
