package com.atlium.restataunt.bar;

import com.atlium.product.Drink;
import com.atlium.product.DrinkType;

public interface Bar {
    Drink createDrink(DrinkType drinkType);
}
